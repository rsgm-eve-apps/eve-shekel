from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from django.conf import settings
from django.conf.urls import url, include
from django.views.generic.base import TemplateView
from rest_framework import routers

from shekel import consumers
from shekel.views.alert import AlertViewSet
from shekel.views.auth import CreateCharacterView, Scopes
from shekel.views.character import CharacterViewSet, EvemailUnsubscribe, CharacterSettingsViewSet
from shekel.views.order import OrderViewSet, OrderWindow, OrderSettingsViewSet
from shekel.views import public

router = routers.SimpleRouter()
router.register(r'characters', CharacterViewSet)
router.register(r'character-settings', CharacterSettingsViewSet)

router.register(r'orders', OrderViewSet)
router.register(r'order-settings', OrderSettingsViewSet)
router.register(r'alerts', AlertViewSet)

# router.register(r'public/item', public.ItemViewSet)
# router.register(r'public/item/{item_id}/orders', public.OrderViewSet)

urlpatterns = [
    url(r'^api/auth/scopes/$', Scopes.as_view()),

    url(r'^api/auth/scopes/$', Scopes.as_view()),
    url(r'^api/auth/sso/$', CreateCharacterView.as_view()),
    url(r'^api/orders/(?P<order_id>\d+)/window/$', OrderWindow.as_view()),
    url(r'^api/public/item/(?P<item_id>\d+)/history/$', public.HistoryViewSet.as_view()),

    url(r'^api/', include(router.urls)),

    url(r'^unsubscribe/(?P<character_id>\d+)/(?P<token>.+)/$', EvemailUnsubscribe.as_view()),

    url(r'^.*', TemplateView.as_view(template_name='index.html', extra_context={'debug': settings.DEBUG})),
]

websocket_urlpatterns = [
    url(r'^ws/character/(?P<character_id>\d+)/$', consumers.CharacterConsumer),
    url(r'^ws/healthcheck/$', consumers.HealthCheckConsumer),
]

application = ProtocolTypeRouter({
    # (http->django views is added by default)

    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(websocket_urlpatterns)
        )
    ),
})
