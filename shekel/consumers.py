import json
import logging

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

logger = logging.getLogger(__name__)


class CharacterConsumer(WebsocketConsumer):
    def connect(self):
        self.character = self.scope['url_route']['kwargs']['character_id']
        self.group_name = 'character-{}'.format(self.character)

        async_to_sync(self.channel_layer.group_add)(
            self.group_name,
            self.channel_name
        )
        print('connected')
        logger.debug('Character consumer connected')
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name,
            self.channel_name
        )
        logger.debug('Character consumer disconnected')


    # Receive message from room group
    def character_update(self, event):
        message = event.get('message', '')

        logger.debug('Sending character updates to {}'.format())
        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))


class HealthCheckConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()
        logger.debug('Health check consumer connected')

    def receive(self, text_data=None, bytes_data=None):
        logger.debug('Health check ping')
        if text_data == 'ping':
            self.send('pong')
        else:
            self.send('hello')

    def disconnect(self, close_code):
        logger.debug('Health check consumer disconnected')
