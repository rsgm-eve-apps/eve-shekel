from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView
from rest_framework import viewsets, serializers, mixins

from shekel.models import Character, EvemailUnsubscribeToken, CharacterSettings


class CharacterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Character
        fields = ('id', 'name')


class CharacterSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CharacterSettings
        fields = ('character', 'eve_mail_alerts', 'eve_mail_recipient')


class CharacterViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = CharacterSerializer
    queryset = Character.objects.all()

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)


class CharacterSettingsViewSet(mixins.RetrieveModelMixin,
                               mixins.UpdateModelMixin,
                               viewsets.GenericViewSet):
    serializer_class = CharacterSettingsSerializer
    queryset = CharacterSettings.objects.all()

    def get_queryset(self):
        return self.queryset.filter(character__user=self.request.user)


class EvemailUnsubscribe(TemplateView):
    """
    View to disable evemail alerts given a character_id and unsubscribe token.
    """
    template_name = 'unsubscribe.html'

    def get(self, request, *args, **kwargs):
        token = kwargs['token']
        character_id = kwargs['character_id']

        # get unsubscribe token
        queryset = EvemailUnsubscribeToken.objects.select_related('character', 'character__settings')
        disable_token = get_object_or_404(queryset, pk=token, character=character_id)

        # disable evemail alerts
        settings = disable_token.character.settings
        settings.eve_mail_alerts = False
        settings.save()

        disable_token.delete()

        return super(EvemailUnsubscribe, self).get(self, request, *args, **kwargs)
