import datetime
import json
import logging
import time
import uuid
from collections import defaultdict
from gevent import sleep
from itertools import repeat

import statistics
from django.conf import settings
from django.db import OperationalError
from django.db.models import Subquery, OuterRef, Exists
from django.db.models.expressions import F
from django.db.models.query_utils import Q
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from eveapi.esi.client import ESI
from eveapi.esi.errors import ESIMaxAgeExceeded, ESIException, ESITimeout
from huey.exceptions import HueyException, TaskException
from redis.client import StrictRedis

from shekel.huey import auth_refresh
from shekel.models import Character, Order, OrderSettings, Location, Region, Item, Alert, Market, MarketUpdate
from shekel.util import chunks, dict_chunks, acquire_lock, delete_lock
from . import GREENLET_QUEUE, THREAD_QUEUE

logger = logging.getLogger(__name__)
redis = StrictRedis(connection_pool=settings.REDIS_POOL)

ESI_CLIENT = ESI(user_agent=settings.ESI_USER_AGENT, auth_callback=auth_refresh.get_auth)

BATCH_SIZE = 5000
MAX_STATION_ID = 2147483647

RUNNING_KEY = settings.REDIS_BASE + '.order_updates.fresh_region_updates'
LOCATION_LAST_OPEN_ORDERS_KEY = settings.REDIS_BASE + '.order_updates.location_open_orders.'
MARKET_TRADE_SET_KEY = settings.REDIS_BASE + '.market_updates.keys'
MARKET_TRADES_BASE_KEY = settings.REDIS_BASE + '.market_updates'


# periodic tasks (sometimes called runner tasks)
# ----------------------------------------------------------------------------------

@GREENLET_QUEUE.db_periodic_task(settings.REGION_ORDER_UPDATE_CRON, name='order_updates.run_region_updates.periodic')
@GREENLET_QUEUE.db_task(name='order_updates.run_region_updates.task')
@GREENLET_QUEUE.lock_task('run_region_updates-lock')
def run_region_updates():
    start_time = time.time()

    regions = list(Region.objects.filter(id__lt=11000000).order_by('id').values_list('id', flat=True))
    logger.info('Scheduling next batch of region updates - {} regions'.format(len(regions)))

    first_run = not redis.get(RUNNING_KEY)
    tasks = [(region, update_region_orders(region, first_run=first_run)) for region in regions]

    # some things depend on region data already existing, so set this after the first run
    # redis is configured to be in-memory, so this gets cleared every restart
    if first_run:
        for region, task in tasks:
            try:
                _new, _updated, _closed = task.get(blocking=True, timeout=3600)
            except ESI:
                continue
        redis.set(RUNNING_KEY, 1)  # set this once done
        logger.info('Finished first run updates for all regions', extra={'duration': time.time() - start_time})


@GREENLET_QUEUE.db_periodic_task(settings.CHARACTER_UPDATE_CRON, name='order_updates.run_character_updates.periodic')
@GREENLET_QUEUE.db_task(name='order_updates.run_character_updates.task')
@GREENLET_QUEUE.lock_task('run_character_updates-lock')
def run_character_updates():
    start_time = time.time()

    if not redis.get(RUNNING_KEY):
        logger.warning('Currently starting up region updates')
        return  # don't run if we are currently starting up region updates

    # get every character with a valid token
    # todo: figure out what happens if we think token is valid, but is actually revoked
    characters = list(Character.objects.filter(
        refresh_token__revoked_permission=False,
    ).select_related(
        'refresh_token',
        'refresh_token__access_token'
    ).iterator())

    logger.info('Scheduling next batch of character updates for {} characters'.format(len(characters)))

    # start calling esi for character orders, collect as list of (character, call_task) pairs
    esi_calls = [(character.pk,
                  ESI_CLIENT._fork(character=character.pk).v2.characters.self.orders(fetch_pages=True))
                 for character in characters]

    # todo: move this processing to a thread queue task if we start to get too many users
    new_order_count = 0
    update_count = 0
    for character_id, esi_call in esi_calls:
        try:
            orders = esi_call.get(blocking=True, timeout=300)
        except ESIException:
            logger.error('Error refreshing character {}'.format(character_id), extra={'character': character_id})
            continue

        # assign existing orders to this character
        existing_orders = Order.objects.filter(order_id__in=[order['order_id'] for order in orders])
        update_count += existing_orders.update(character_id=character_id)

        # create any orders that do not exist yet
        existing_order_ids = set(existing_orders.values_list('order_id', flat=True))
        new_orders = [order for order in orders if order['order_id'] not in existing_order_ids]
        new_order_count += len(new_orders)
        if new_orders:
            create_orders(new_orders, character_id=character_id)

    dt = time.time() - start_time
    logger.info('Finished updates for all characters', extra={
        'characters': len(esi_calls),
        'new_orders': new_order_count,
        'updated_orders': update_count,
        'duration': dt
    })


@GREENLET_QUEUE.db_periodic_task(settings.STRUCTURE_ORDER_UPDATE_CRON,
                                 name='order_updates.run_structure_updates.periodic')
@GREENLET_QUEUE.db_task(name='order_updates.run_structure_updates.task')
@GREENLET_QUEUE.lock_task('run_structure_updates-lock')
def run_structure_updates():
    logger.info('Starting structure updates')

    if not redis.get(RUNNING_KEY):
        logger.warning('Currently starting up region updates')
        return  # don't run if we are currently starting up region updates

    start_time = time.time()
    structures = get_structures()
    logger.info('Got structures', extra={
        'structures': structures.count(),
        'duration': time.time() - start_time,
    })

    # make esi calls
    structure_calls = [(
        structure,
        ESI_CLIENT._fork(character=structure.character_id).v1.markets.structures[structure.id](
            fetch_pages=True,
            cache=False
        )
    ) for structure in structures]

    order_updates = defaultdict(list)
    structure_call_retries = []

    # give all structures 150 seconds to complete, in 5 passes of increasing wait time
    # I'm not sure if this really helps with anything
    for i in range(5):
        structure_call_retries = []
        if len(structure_calls) == 0:
            break  # no need to wait more

        sleep(10 * (i + 1))

        for structure, structure_call in structure_calls:
            try:
                orders = structure_call.get()
            except ESIException:
                continue

            if not orders:
                structure_call_retries.append((structure, structure_call))
            elif len(orders):
                structure_id = orders[0]['location_id']
                order_updates[structure_id] += orders
        structure_calls = structure_call_retries

    logger.info('Got ESI structure data', extra={
        'structure_calls': len(structures),
        'structure_call_retries': len([structure for structure, _ in structure_call_retries]),
    })

    if len(order_updates) == 0:
        logger.error('ESI structure calls got no data')  # this is almost always an error
        return

    for structure_ids, orders in dict_chunks(order_updates, 2000):
        update_structure_orders(structure_ids, orders)


@GREENLET_QUEUE.db_periodic_task(settings.LOCATION_REFRESH_CRON, name='order_updates.run_location_refresh.periodic')
@GREENLET_QUEUE.db_task(name='order_updates.run_location_refresh.task')
@GREENLET_QUEUE.lock_task('run_location_refresh-lock')
def run_location_refresh():
    logger.debug('Starting location refresh')

    # get all stations
    stations = list(Location.objects.filter(is_structure=False))

    start_time_b = time.time()
    structures = list(get_structures())
    logger.info('Got structures', extra={
        'structures': structures,
        'duration': time.time() - start_time_b,
    })

    logger.info('location refresh, got {} stations and {} structures'.format(
        len(stations),
        len(structures),
    ), extra={
        'stations': len(stations),
        'structures': len(structures)
    })

    batch_size = 500
    locations = stations + structures
    for location_chunk in chunks(locations, batch_size):
        refresh_locations_info(location_chunk)


# runs 4 times per day
@GREENLET_QUEUE.db_periodic_task(settings.RUN_EXPIRY_ALERT_CHECKS,
                                 name='order_updates.run_expiry_alert_checks.periodic')
@GREENLET_QUEUE.db_task(name='order_updates.run_expiry_alert_checks.task')
@GREENLET_QUEUE.lock_task('run_expiry_alert_checks-lock')
def run_expiry_alert_checks():
    start_time = time.time()

    orders = list(Order.objects.filter(
        character__isnull=False,
        settings__expiry_alerts=True,
        settings__expiry_alert_happened=False,
    ).select_related('settings').iterator())

    logger.info('Starting run expiry alert checks')

    for order in orders:
        if order.issued + timezone.timedelta(days=order.settings.expiry_limit) < timezone.now():
            Alert.objects.create(order=order, change=Alert.VOLUME)
            order.volume_alert_happened = False
            order.save()

    dt = time.time() - start_time
    logger.info('Finished expiry alert checks', extra={
        'new_alerts': len(orders),
        'duration': dt
    })


@GREENLET_QUEUE.db_periodic_task(settings.RUN_MARKET_UPDATES, name='order_updates.run_market_updates.periodic')
@GREENLET_QUEUE.db_task(name='order_updates.run_market_updates.task')
@GREENLET_QUEUE.lock_task('run_market_updates-lock')
def run_market_updates():
    start_time = time.time()

    logger.info('Starting market updates for all markets with changes')

    markets_keys = list(redis.smembers(MARKET_TRADE_SET_KEY))
    redis.delete(MARKET_TRADE_SET_KEY)

    for key_chunk in chunks(markets_keys, 1000):
        calculate_market_updates(key_chunk)


# individual tasks
# ----------------------------------------------------------------------------------

# called when someone adds a character
@GREENLET_QUEUE.db_task(name='order_updates.add_character_orders')
def add_character_orders(character_id):
    start_time = time.time()

    # start calling esi for character orders, collect as list of (character, call_task) pairs
    orders = ESI_CLIENT._fork(character=character_id).v2.characters.self.orders(fetch_pages=True)

    update_count = Order.objects.filter(order_id__in=[order['order_id'] for order in orders]) \
        .update(character_id=character_id)

    if update_count < len(orders):
        create_orders(orders, character_id=character_id)

    dt = time.time() - start_time
    logger.info('Finished adding character orders', extra={'duration': dt})


@THREAD_QUEUE.db_task(name='order_updates.update_region_orders')
def update_region_orders(region, first_run=False):
    # we need to use locking to ensure this is only being run once at a time
    # we don't want to be updating jita on 7 consumers
    lock_key = settings.REDIS_BASE + '.region_update.' + str(region)
    lock_value = str(uuid.uuid4())

    if not acquire_lock(redis, lock_key, lock_value, 900):  # clear lock after a 15 minutes
        logger.warning('Could not obtain lock for region update', extra={'region': region})
        return 0, 0, 0  # since we block on this task, we need to return the expected tuple

    logger.debug('Starting region update', extra={'region': region})

    max_age = None if first_run else 60

    start_time = time.time()
    try:
        region_orders = ESI_CLIENT.v1.markets[region].orders(
            fetch_pages=True,
            cache=False,
        ).get(blocking=True, timeout=60)
    except ESIException as ex:
        logger.error('Region ESI error', extra={'exception': ex.error})
        delete_lock(redis, lock_key, lock_value)  # clean up region update lock
        return 0, 0, 0  # nothing to update

    logger.info('Got region update orders from esi', extra={
        'duration': time.time() - start_time,
        'region': region,
        'region_esi_orders': len(region_orders)
    })

    start_time = time.time()  # reset time, so were not counting time blocking the thread
    new_orders, updated_orders, closed_orders = update_orders(region_orders, region_id=region)

    Location.objects.filter(region_id=region).update(last_update=datetime.datetime.now())

    # clean up region update lock
    delete_lock(redis, lock_key, lock_value)

    logger.info('Finished region update', extra={
        'duration': time.time() - start_time,
        'region': region,
        'new_orders': new_orders,
        'updated_orders': updated_orders,
        'closed_orders': closed_orders
    })

    return new_orders, updated_orders, closed_orders


@THREAD_QUEUE.db_task(name='order_updates.update_structure_orders')
def update_structure_orders(structure_ids, orders):
    start_time = time.time()
    logger.info('Starting structure updates', extra={'structures': structure_ids, 'structures_esi_orders': len(orders)})

    new_orders, order_updates, closed_orders = update_orders(orders, structure_ids=structure_ids)

    Location.objects.filter(pk__in=structure_ids).update(last_update=datetime.datetime.now())

    dt = time.time() - start_time
    logger.info('Finished structures update', extra={
        'duration': dt,
        'structures': structure_ids,
        'new_orders': new_orders,
        'updated_orders': order_updates,
        'closed_orders': closed_orders
    })
    return new_orders, order_updates, closed_orders


# does not need to be a task
def update_orders(orders, region_id=None, structure_ids=None):
    """
    Update db orders given a list of orders from the esi.
    We also need a location filter to find which orders we can close.

    :param orders: ESI order list
    :param region_id: id of the region if we are updating a region, used for closing orders
    :param structure_ids: id of stations if we are updating stations, used for closing orders
    :return:
    """

    logger.debug('Starting order updates for {} orders'.format(len(orders)), extra={'orders': len(orders)})

    # check if this is the run after startup
    is_running = redis.get(RUNNING_KEY)

    # object storage for bulk queries
    new_orders = []  # newly created Order objects
    saved_order_updates = []  # updates to saved orders
    new_alerts = []  # newly created Alert objects
    market_changes = set()  # tracks unique (type_id, location_id) to make price alerts
    market_trades = defaultdict(lambda: {'buy': [], 'sell': []})  # contains
    current_datetime = datetime.datetime.now()
    open_order_ids = set()

    # chunk orders so we don't get too many back at once, greatly reducing memory use
    for order_chunk in chunks(orders, 2000):
        try:
            saved_open_orders = Order.objects.filter(order_id__in={o['order_id'] for o in order_chunk}).only(
                'price',
                'volume_remain',
                'market_id',
                'character_id',
                'settings',
            ).select_related('settings').in_bulk()
        except OperationalError as ex:
            logger.error('DB closed connection while, retrying task', extra={
                'region_id': region_id,
                'structure_ids': structure_ids,
                'orders': len(orders)
            })
            raise ex  # should retry the task

        for order in order_chunk:
            order_id = order['order_id']
            open_order_ids.add(order_id)
            saved_order = saved_open_orders.get(order_id, None)

            if saved_order:
                # there may be a better way to compare decimal and float objects
                price = '{:.2f}'.format(order['price'])
                volume_remain = order['volume_remain']

                # if price or volume changed, we need to update the order, otherwise continue
                price_changed = str(saved_order.price) != price
                volume_changed = saved_order.volume_remain != volume_remain

                if price_changed or volume_changed:
                    if price_changed:
                        market_changes.add(saved_order.market_id)

                    if volume_changed:
                        market_trades[saved_order.market_id]['buy' if order['is_buy_order'] else 'sell'].append(
                            ','.join([
                                price,
                                str(saved_order.volume_remain - volume_remain)  # volume change
                            ]))

                    saved_order.price = price
                    saved_order.volume_remain = volume_remain
                    saved_order_updates.append(saved_order)

                    # check if this order's volume remaining is less than settings.volume_limit, if set
                    order_settings = saved_order.settings
                    if saved_order.character_id \
                            and volume_changed \
                            and order_settings.volume_alerts \
                            and not order_settings.volume_alert_happened \
                            and order_settings.volume_limit >= saved_order.volume_remain:
                        new_alerts.append(Alert(order=saved_order, change=Alert.VOLUME))
                        order_settings.volume_alert_happened = False
                        order_settings.save()

            else:
                new_orders.append(order)

    logger.debug('finished processing esi order updates')

    closed_order_ids = None
    if region_id:
        open_orders_key = LOCATION_LAST_OPEN_ORDERS_KEY + str(region_id)
        temp_open_orders_key = open_orders_key + '.temp'

        # store new open orders here temporarily
        if len(open_order_ids):
            redis.sadd(temp_open_orders_key, *open_order_ids)
        logger.debug('added {} new open order ids to redis'.format(len(open_order_ids)))

        # if this is the first run, we have to populate redis with previously saved orders
        if not is_running:
            saved_order_ids = list(Order.objects.filter(
                is_open=True,
                market__location__region_id=region_id,
                market__location__is_structure=False,
            ).values_list('order_id', flat=True).iterator())

            if len(saved_order_ids):
                redis.sadd(open_orders_key, *saved_order_ids)
            logger.debug('first run, added all {} region open order ids from db'.format(len(saved_order_ids)))

        closed_order_ids = {int(order_id) for order_id
                            in redis.sdiff(open_orders_key, temp_open_orders_key)}
        wrongly_closed_order_ids = {int(order_id) for order_id  # orders that are closed but shouldn't be
                                    in redis.sdiff(temp_open_orders_key, open_orders_key)}
        logger.debug('calculated closed order ids')

        if len(open_order_ids):
            redis.rename(temp_open_orders_key, open_orders_key)
        logger.debug('replaced old open orders with new open orders')

    elif structure_ids:
        # this is a bit tougher to get a redis key for
        # since there are less orders, we'll just do this using the db

        # calculate and close no longer open orders
        # get all saved orders, used for updating orders and calculating closed orders
        saved_open_order_ids = set(Order.objects.filter(
            is_open=True,
            market__location_id__in=structure_ids,
        ).values_list('order_id', flat=True).iterator())
        logger.debug('got saved station orders')

        # calculate closed orders
        closed_order_ids = saved_open_order_ids - open_order_ids
        wrongly_closed_order_ids = open_order_ids - saved_open_order_ids

    else:
        logger.error('Both region_id and station filter are None')
        raise Exception('Both region_id and station filter are None')

    logger.debug('starting to close orders')
    closed_qs = Order.objects.filter(order_id__in=closed_order_ids)

    closed_order_count = closed_qs.update(is_open=False, closed_date=current_datetime)
    logger.debug('closed {} orders'.format(closed_order_count))

    reopened_order_count = Order.objects.filter(order_id__in=wrongly_closed_order_ids).update(
        is_open=True,
        closed_date=None,
    )
    if reopened_order_count:
        logger.warning('Reopened wrongly closed orders'.format(reopened_order_count), extra={
            'reopened_orders': reopened_order_count,
            'update_filter_region': region_id,
            'update_filter_structures': structure_ids,
            'esi_order_count': len(orders),
            'closed_orders': closed_order_count
        })

    # create closed order alerts
    for order_id in closed_qs.filter(character__isnull=False).values_list('order_id', flat=True).iterator():
        new_alerts.append(Alert(order_id=order_id, change=Alert.CLOSED))
    logger.debug('finished creating closed order alerts')

    # add closed orders to market trades
    for market, is_buy_order, price, volume_remain in closed_qs.values_list('market', 'is_buy_order',
                                                                            'price', 'volume_remain'):
        market_trades[market]['buy' if is_buy_order else 'sell'].append(
            ','.join([
                str(price),
                str(volume_remain)
            ]))
    logger.debug('finished adding closed orders to market trades')

    if len(new_orders) > 0:
        market_changes.update(create_orders(new_orders, update_best_prices=False))  # update leftover orders

    Order.objects.bulk_update(saved_order_updates, fields=['price', 'volume_remain'], batch_size=5000)
    logger.debug('finished updating orders')
    Alert.objects.bulk_create(new_alerts, batch_size=5000)
    logger.debug('finished creating alerts')

    # store trades in redis lists, if not the first update
    if is_running:
        logger.debug('storing trades for {} markets'.format(len(market_trades)))
        for market, trades in market_trades.items():
            key = '{}.{}'.format(MARKET_TRADES_BASE_KEY, market)
            redis.sadd(MARKET_TRADE_SET_KEY, key)
            redis.expire(MARKET_TRADE_SET_KEY, 60 * 25)
            if len(trades['buy']):
                redis.rpush(key + '.buy', *trades['buy'])
                redis.expire(key + '.buy', 60 * 25)
            if len(trades['sell']):
                redis.rpush(key + '.sell', *trades['sell'])
                redis.expire(key + '.sell', 60 * 25)

    # do stat calculations last for most recent data
    if len(market_changes):
        # update best prices for all (item,location) pair changes
        update_item_locations_best_prices(market_changes)

    # update_region_order_stats(region)

    buy_trade_count = sum([len(trades['buy']) for trades in market_trades.values()])
    sell_trade_count = sum([len(trades['sell']) for trades in market_trades.values()])

    logger.info('Finished updating orders', extra={
        'region': region_id,
        'structure_ids': structure_ids,
        'new_orders': len(new_orders),
        'saved_order_updates': len(saved_order_updates),
        'closed_order_count': closed_order_count,
        'reopened_order_count': reopened_order_count,
        'buy_trade_count': buy_trade_count,
        'sell_trade_count': sell_trade_count
    })

    return len(new_orders), len(saved_order_updates), closed_order_count


def create_orders(orders, character_id=None, update_best_prices=True):
    start_time = time.time()
    logger.debug('started creating {} orders'.format(len(orders)))

    new_locations = []
    new_orders = []
    new_order_updates = []
    new_order_settings = []
    market_changes = set()

    saved_locations = Location.objects.in_bulk([order['location_id'] for order in orders])
    logger.debug('got saved locations')
    saved_items = Item.objects.in_bulk([order['type_id'] for order in orders])
    logger.debug('got saved items')

    for order in orders:
        location = saved_locations.get(order['location_id'], None)
        item = saved_items.get(order['type_id'], None)

        if not location:  # new locations are more frequent than new items, move that to a task
            location = Location.objects.create(
                id=order['location_id'],
                is_structure=order['location_id'] > MAX_STATION_ID
            )
            logger.debug('created new location {}'.format(location.id))
            new_locations.append(location)

            # make sure we don't try to create multiple of the same location,
            # and make sure we have a location object to give the order, if location was created in this batch
            saved_locations[location.id] = location

        if not item:  # if its a new item, create the item, should be very rare
            # this can raise a DatastoreTimeout, but it will be rare enough not to impact anything
            item_name = ESI_CLIENT.v2.universe.names.post(
                data=json.dumps([order['type_id']]))  # todo: should probably block

            item = Item.objects.create(id=order['type_id'], name=item_name)
            logger.debug('created new item {}'.format(item.id))

        try:
            market = Market.objects.get(location=location, item=item)
        except Market.DoesNotExist:
            market = Market.objects.create(location=location, item=item)

        new_order = Order(
            order_id=order['order_id'],
            character_id=character_id,
            market=market,

            issued=parse_datetime(order['issued']),
            duration=order['duration'],
            is_buy_order=order['is_buy_order'],
            volume_remain=order['volume_remain'],
            volume_total=order['volume_total'],
            min_volume=order['min_volume'],
            price=order['price'],
            order_range=order['range'],
        )

        order_settings = OrderSettings(order=new_order)

        new_orders.append(new_order)
        new_order_settings.append(order_settings)

        market_changes.add(market.id)

    logger.debug('finished processing new orders')

    # schedule a task to lookup other location info later, we don't need to wait for this info
    if len(new_locations):
        refresh_locations_info(new_locations, character_id)
        logger.debug('finished refreshing new locations')

    if update_best_prices and len(market_changes):
        # update best prices for all market changes
        update_item_locations_best_prices(market_changes)
        logger.debug('finished updating markets with new orders')

    # now, start creating Order, OrderUpdate, and OrderSettings objects
    Order.objects.bulk_create(new_orders, ignore_conflicts=True, batch_size=5000)
    logger.debug('finished creating new orders')
    OrderSettings.objects.bulk_create(new_order_settings, ignore_conflicts=True, batch_size=5000)
    logger.debug('finished creating new order settings')

    dt = time.time() - start_time
    logger.info('Finished creating orders', extra={
        'duration': dt,
        'created_orders': len(orders),
        'new_locations': len(new_locations)
    })

    return market_changes


@GREENLET_QUEUE.db_task(name='order_updates.refresh_locations_info')
def refresh_locations_info(locations, character_id=None):
    start_time = time.time()
    logger.debug('refreshing {} locations'.format(len(locations)), extra={'locations': len(locations)})

    # station locations
    station_calls = [(location, ESI_CLIENT.v2.universe.stations[location.id]())
                     for location in locations if not location.is_structure and not location.name]

    # structure locations
    # all private structures need to be annotated with a character_id to use to look them up
    structure_calls = [(location,
                        ESI_CLIENT._fork(
                            character=character_id or location.character_id
                        ).v2.universe.structures[location.pk]())
                       for location in locations
                       if location.is_structure and (character_id or hasattr(location, 'character_id'))]

    logger.debug('refreshing {} stations and {} structures'.format(len(station_calls), len(structure_calls)))

    constellation_calls = {}
    locations_without_regions = defaultdict(list)

    def constellation_call(solar_system_id):
        if solar_system_id not in constellation_calls:
            constellation_calls[solar_system_id] = ESI_CLIENT.v4.universe.systems[solar_system_id]()

    location_updates = []
    for station, station_call in station_calls:
        location_updates.append(station)

        try:
            station_data = station_call.get(blocking=True, timeout=60)
        except Exception:

            continue

        if not isinstance(station_data, dict):
            logger.error('Could not get station information, got: {}'.format(station_data))

        else:
            station.name = station_data['name']
            station.is_structure = False

            system_id = station_data['system_id']
            constellation_call(system_id)
            locations_without_regions[system_id].append(station)

    # we won't update every structure, only public and *some* private structures
    for structure, structure_call in structure_calls:
        location_updates.append(structure)

        try:
            data = structure_call.get(blocking=True, timeout=60)
        except ESIException:
            continue

        if not isinstance(data, dict):
            logger.error('Could not get structure information, got: {}'.format(data))

        else:
            structure.name = data['name']
            system_id = data['solar_system_id']

            constellation_call(system_id)
            locations_without_regions[system_id].append(structure)

        structure.is_structure = True

    # turn constellation calls into region calls
    region_calls = {}
    for system, constellation_call in constellation_calls.items():
        try:
            data = constellation_call.get(blocking=True, timeout=60)
        except HueyException:
            continue

        # todo: should probably block
        region_calls[system] = ESI_CLIENT.v1.universe.constellations[data['constellation_id']]()

    # get actual region ids and assign them to locations without regions
    for system, locations in locations_without_regions.items():
        if system in region_calls:
            try:
                region = region_calls[system].get(blocking=True, timeout=60)['region_id']
            except HueyException:
                continue

            for station in locations:
                station.region_id = region

    Location.objects.bulk_update(
        location_updates,
        fields=['name', 'region_id', 'is_structure']
    )

    dt = time.time() - start_time
    logger.info('Finished refreshing location info', extra={
        'duration': dt,
        'updated_locations': len(location_updates)
    })


def update_item_locations_best_prices(market_ids):
    start_time = time.time()

    logger.debug('starting best price updates, got {} market IDs'.format(len(market_ids)))

    # find best priced orders with characters before the update
    last_best_prices = list(Order.objects.filter(
        character__isnull=False,
        market_id__in=market_ids,
    ).annotate(
        best_buy_order=Subquery(Market.objects.filter(id=OuterRef('market_id')).values('best_buy_order')[:1]),
        best_sell_order=Subquery(Market.objects.filter(id=OuterRef('market_id')).values('best_sell_order')[:1]),
    ).filter(
        Q(order_id=F('best_buy_order')) | Q(order_id=F('best_sell_order'))
    ).values_list('order_id', flat=True).iterator())
    logger.debug('got last best prices')

    # calculate and update current best orders
    Market.objects.filter(
        id__in=market_ids,
    ).annotate(
        new_best_sell_order=Subquery(
            Order.objects.filter(
                is_open=True,
                is_buy_order=False,
                market_id=OuterRef('id')
            ).order_by('price').values('order_id')[:1]
        ),
        new_best_buy_order=Subquery(
            Order.objects.filter(
                is_open=True,
                is_buy_order=True,
                market_id=OuterRef('id')
            ).order_by('-price').values('order_id')[:1]
        ),
    ).update(
        best_sell_order=F('new_best_sell_order'),
        best_buy_order=F('new_best_buy_order'),
    )
    logger.debug('finished best price updates')

    # get last best orders that are no longer the best order
    changed_best_prices = Order.objects.filter(
        order_id__in=last_best_prices
    ).annotate(
        best_buy_order=Subquery(Market.objects.filter(id=OuterRef('market_id')).values('best_buy_order')[:1]),
        best_sell_order=Subquery(Market.objects.filter(id=OuterRef('market_id')).values('best_sell_order')[:1]),
    ).exclude(
        Q(order_id=F('best_buy_order')) | Q(order_id=F('best_sell_order'))
    ).values_list('order_id', flat=True).iterator()
    logger.debug('got orders that are no longer the best price')

    # create price alerts for all orders that are no longer the best price
    new_alerts = [Alert(order_id=order_id, change=Alert.PRICE) for order_id in changed_best_prices]
    Alert.objects.bulk_create(new_alerts)

    dt = time.time() - start_time
    logger.info('Finished best price updates', extra={
        'duration': dt,
        'market_ids': len(market_ids),
        'new_alerts': len(new_alerts)
    })


@THREAD_QUEUE.db_task(name='order_updates.calculate_market_updates')
def calculate_market_updates(market_keys):
    start_time = time.time()
    logger.debug('Starting market update calculations', extra={'markets': len(market_keys)})

    market_updates = []
    for market_key in market_keys:
        key = market_key.decode('utf-8')
        buy_key = key + '.buy'
        sell_key = key + '.sell'

        buy_trades = []
        sell_trades = []

        for string in redis.lrange(buy_key, 0, 10000000):
            price, volume = tuple(string.decode('utf-8').split(','))
            buy_trades.append((float(price), int(volume)))

        for string in redis.lrange(sell_key, 0, 10000000):
            price, volume = tuple(string.decode('utf-8').split(','))
            sell_trades.append((float(price), int(volume)))

        # delete lists
        redis.delete(buy_key)
        redis.delete(sell_key)

        market_update = MarketUpdate()
        market_update.market_id = int(key.split('.')[-1])  # market id is the last part of the key

        def median(l):
            sorted_list = sorted(l, key=lambda x: x[0])
            if len(sorted_list) == 0:
                return 0
            if len(sorted_list) == 1:
                return sorted_list[0][0]

            i = 0
            j = len(sorted_list) - 1
            head = sorted_list[i][1]
            tail = sorted_list[j][1]

            for _ in range(len(sorted_list)):
                if head > tail:
                    head -= tail
                    j -= 1
                    tail = sorted_list[j][1]

                elif head < tail:
                    tail -= head
                    i += 1
                    head = sorted_list[i][1]

                else:
                    if i + 1 == j:
                        return (sorted_list[i][0] + sorted_list[j][0]) / 2

                    i += 1
                    j -= 1
                    head = sorted_list[i][1]
                    tail = sorted_list[j][1]

                if i == j:
                    return sorted_list[i][0]

            logger.error('Median not found for market update')
            return 0

        if len(buy_trades):
            market_update.buy_trade_count = len(buy_trades)
            market_update.buy_volume = sum([volume for _, volume in buy_trades])
            market_update.buy_value = sum([price * volume for price, volume in buy_trades])
            market_update.buy_mean = statistics.mean([price for price, _ in buy_trades])
            market_update.buy_median = median(buy_trades)

        if len(sell_trades):
            market_update.sell_trade_count = len(sell_trades)
            market_update.sell_volume = sum([volume for _, volume in sell_trades])
            market_update.sell_value = sum([price * volume for price, volume in sell_trades])
            market_update.sell_mean = statistics.mean([price for price, _ in sell_trades])
            market_update.sell_median = median(sell_trades)

        if len(buy_trades) or len(sell_trades):
            all_prices = [price for price, _ in buy_trades + sell_trades]

            market_update.high_price = max(all_prices)
            market_update.low_price = min(all_prices)

            market_update.total_mean = statistics.mean(all_prices)
            market_update.total_median = median(buy_trades + sell_trades)

        market_updates.append(market_update)

    MarketUpdate.objects.bulk_create(market_updates, batch_size=5000)
    dt = time.time() - start_time
    logger.info('Finished market update calculations', extra={
        'duration': dt,
        'market_count': len(market_keys),
        'updated_markets': len(market_updates)
    })
    return True


@THREAD_QUEUE.db_task(name='order_updates.update_region_order_stats')
def update_region_order_stats(region):
    pass


def get_structures(extra_filter=None):
    # subquery to select a character to query private structures with
    newest_order = Order.objects.filter(
        market__location_id=OuterRef('id'),
        is_open=True,
        character__isnull=False
    ).order_by('-pk')

    # get private structures that we can probably access(based on open character orders)
    return Location.objects.filter(**(extra_filter or {})).annotate(
        available=Exists(Order.objects.filter(
            market__location_id=OuterRef('id'),
            is_open=True,
            character__isnull=False
        ))
    ).filter(  # this is a lot more complex to find
        is_structure=True,
        available=True,
    ).annotate(
        character_id=Subquery(newest_order.values('character_id')[:1])
    ).distinct()
