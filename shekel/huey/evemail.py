import logging

import json

from collections import defaultdict
from django.template.loader import get_template
from eveapi.esi.client import ESI

from shekel.huey import auth_refresh
from shekel.models import EvemailUnsubscribeToken, Alert
from django.conf import settings
from . import GREENLET_QUEUE, THREAD_QUEUE

logger = logging.getLogger(__name__)


@GREENLET_QUEUE.db_task(name='evemail.evemail_alert_summary')
def evemail_alert_summary(character_id):
    client = ESI(user_agent=settings.ESI_USER_AGENT, auth_callback=auth_refresh.get_auth)
    mail_client = client._fork(
        character=settings.EVEMAIL_CHARACTER_ID
    )

    alerts = Alert.objects.filter(
        order__character=character_id,
        cleared=False,
        evemailed=False,
    ).order_by('order__issued').select_related('order', 'order__character', 'order__character__settings')

    if alerts.count() == 0:
        return

    character = alerts.first().order.character
    unsubscribe = EvemailUnsubscribeToken.objects.create(character=character)

    context = {
        'alerts': defaultdict(list),
        'character': character,
        'unsubscribe_link': unsubscribe.get_link(),
    }

    for alert in alerts:
        order = alert.order
        order_status = alert.ORDER_STATES[alert.change]

        station = None
        structure = None
        system = None

        # order in a station
        if not order.is_structure:
            station = client.v2.universe.stations[order.location_id]()
            system = client.v4.universe.systems[station['system_id']]()

        # order in an authorized structure
        elif order.location != settings.UNAUTHORIZED_STRUCTURE:
            structure = client._fork(character=character.id).v2.universe.structures[order.location_id]()
            system = client.v4.universe.systems[structure['solar_system_id']]()

        context['alerts'][order.location_id].append({
            'order_status': order_status,
            'order': order,
            'station': station,
            'structure': structure,
            'system': system,
            'is_volume_alert': alert.change == alert.VOLUME,
        })

    context['alerts'] = [sorted(location_alerts, key=lambda n: n['order'].item_name)
                         for location_alerts in context['alerts'].values()]

    # print(context['alerts'].items())
    subject = 'EVE Shekel - {} has {} Orders with notifications'.format(character.name, len(alerts))
    body = get_template('evemail.html').render(context)

    data = json.dumps({
        'approved_cost': 0,
        'body': body,
        'recipients': [
            {
                'recipient_id': character.settings.eve_mail_recipient.id,
                'recipient_type': 'character'
            }
        ],
        'subject': subject
    })

    logger.debug('sent evemail: {}'.format(data))

    # ratelimit seems to be 1 every 10 seconds
    mail_client.v1.characters[settings.EVEMAIL_CHARACTER_ID].mail.post(data=data)

    alerts.update(evemailed=True)
