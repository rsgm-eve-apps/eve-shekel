from django.conf import settings
from redis import ConnectionPool

from shekel.util import Queue
from .common import *  # noqa

DEBUG = True  # helps with memory issues on large db calls
SECRET_KEY = env("DJANGO_SECRET_KEY", default='d$z&xj$!cys1iz21^*eb5-c+7p)-_)^ob)y1ak%+zikeb_gvv#')

ALLOWED_HOSTS = ['localhost', '0.0.0.0']

# ESI - local development keys only
CLIENT_ID = '3a4e21ff7eea43f8b7d866919d43cecd'
CLIENT_SECRET = 'gGDhhIPYRSIdHDa8oeeoat0wOttRCuU3ighA33r0'

ESI_USER_AGENT = 'eve-shekel.com DEVELOPMENT (by Rsgm Vaille, rsgm.eve@gmail.com)'
DOMAIN = 'http://localhost:8080'

# periodic task cron overrides
# disable region updates, too much data for local testing
REGION_ORDER_UPDATE_CRON = lambda _: False

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DATABASES = {
    'default': {
        # docker run --rm --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 5432:5432  postgres
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'docker',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}
REDIS_POOL = ConnectionPool(
    host='localhost',
    port=6379,
    max_connections=1000
)

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('localhost', 6379)],
        },
    },
}

REST_FRAMEWORK = {
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
    ),

    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'shekel.authentication.CharacterAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),

    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),

    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.ScopedRateThrottle',
    ),

    'DEFAULT_THROTTLE_RATES': {
        'direct_esi_call': '1/second',
    }
}

# HUEY QUEUES
HUEY_THREAD = {
    'huey': Queue(
        name='shekel.huey.thread',
        blocking=False,
        result_store=True,
        store_errors=False,
        global_registry=False,
        connection_pool=REDIS_POOL,
    ),
    'consumer': {
        'logfile': '/dev/null',
        'workers': 1,
        'worker_type': 'process',
        'backoff': 2,  # Exponential backoff using this rate, -b.
        'max_delay': 20.0,  # Max possible polling interval, -m.
        'periodic': True,  # Enable crontab feature.
        'check_worker_health': True,  # Enable worker health checks.
        'health_check_interval': 1,  # Check worker health every second.
    },
}
HUEY_GREENLET = {
    'huey': Queue(
        name='shekel.huey.greenlet',
        blocking=False,
        result_store=True,
        store_errors=False,
        global_registry=False,
        connection_pool=REDIS_POOL,
    ),
    'consumer': {
        'logfile': '/dev/null',
        'workers': 800,
        'worker_type': 'greenlet',
        'backoff': 2,  # Exponential backoff using this rate, -b.
        'max_delay': 20.0,  # Max possible polling interval, -m.
        'periodic': True,  # Enable crontab feature.
        'check_worker_health': True,  # Enable worker health checks.
        'health_check_interval': 1,  # Check worker health every second.
    },
}
EVEAPI_HUEY_INSTANCE = HUEY_GREENLET['huey']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'simple': {
            'format': '{levelname} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'shekel': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'channels': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'rest_framework': {
            'handlers': ['console'],
            'level': 'INFO',
        },
    }
}
