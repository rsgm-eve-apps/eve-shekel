import json
import requests
from django.conf import settings
from django.core.management.base import BaseCommand

from shekel.models import Region, Location


class Command(BaseCommand):
    help = 'Downloads region data from ESI.'

    def handle(self, *args, **options):
        response = requests.get(settings.ESI_BASE_URL + '/v1/universe/regions/')
        response.raise_for_status()
        region_ids = response.json()

        response = requests.post(settings.ESI_BASE_URL + '/v2/universe/names/', data=json.dumps(region_ids))
        response.raise_for_status()

        Region.objects.bulk_create([Region(id=region['id'], name=region['name'])
                                    for region in response.json()], ignore_conflicts=True)

        print('finished populating regions')
