from urllib import parse

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.crypto import get_random_string
from eveapi.sso import authorize, verify

from shekel.models import Character, RefreshToken, AccessToken


class Command(BaseCommand):
    help = 'Authenticate character through esi with evemail permissions.'

    def handle(self, *args, **options):
        state = get_random_string(length=24)
        redirect_url = settings.DOMAIN + '/add-character/redirect'
        parameters = '&'.join([
            'response_type=code',
            'redirect_uri=' + parse.quote(redirect_url),
            'client_id=' + parse.quote(settings.CLIENT_ID),
            'state=' + state,
            'scope=esi-mail.send_mail.v1 esi-markets.read_character_orders.v1 esi-markets.structure_markets.v1 esi-universe.read_structures.v1',
        ])

        url = 'https://login.eveonline.com/oauth/authorize?' + parameters
        print('login here:')
        print('  ' + url)
        print('\npaste url here:')
        response = input('  ')

        query = parse.urlparse(response).query
        response = parse.parse_qs(query)
        returned_state = response['state'][0]
        code = response['code'][0]

        if returned_state != state:
            print(state)
            print(returned_state)
            raise Exception('sso state mismatch')

        tokens = authorize(code)
        character_info = verify(tokens['access_token'], tokens['token_type'])

        character, _ = Character.objects.update_or_create(  # update if character is on another user
            id=character_info['CharacterID'],
            defaults={
                'name': character_info['CharacterName'],
            }
        )

        refresh_token, _ = RefreshToken.objects.update_or_create(
            character=character,
            defaults={
                'refresh_token': tokens['refresh_token'],
                'token_type': tokens['token_type'],
                'revoked_permission': False,
                'scopes': character_info['Scopes'],
                'character_for': character_info['TokenType'],
                'character_owner_hash': character_info['CharacterOwnerHash'],
            }
        )

        access_token, _ = AccessToken.objects.update_or_create(
            refresh_token=refresh_token,
            defaults={
                'access_token': tokens['access_token'],
                'expires_in': tokens['expires_in'],
            }
        )

        print('evemail character added successfully')
