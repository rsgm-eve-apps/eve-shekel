#!/usr/bin/env bash

mkdir dist || true

if [[ ! -z "${ZIP_DESTINATION}" ]]; then
    tar --exclude-vcs --exclude-vcs-ignores --exclude='./dist' \
         -zcvf \
        $ZIP_DESTINATION \
        . ./.env.sh
fi