#!/usr/bin/env bash

. .env.sh

export REDIS_PORT_6379_TCP_ADDR=localhost

python3 ./manage.py run_huey HUEY_GREENLET
