const BASE_URL = process.env.NODE_ENV !== 'production' ? 'ws://localhost:5000/ws' : 'wss://www.eve-shekel.com/ws';

export default {
  install: function (Vue, options) {
    const websockets = [];

    // 4. add an instance method
    Vue.sockets = {

      /**
       * Listens to a websocket path for messages.
       *
       * @param path path to connect to
       * @param callback function to call on new messages
       */
      listener(path, callback) {
        const ws = new WebSocket(BASE_URL + path);
        websockets.push(ws);

        ws.onmessage = callback
      }
    }
  }
}
